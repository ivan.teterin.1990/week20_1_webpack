import { validate } from './email-validator.js';

let instance = null;    

class SingleSection {
    constructor (type) {
        if (!instance) {
            this.section = createSection(type);
            instance = this;
        }
        else {
            return instance;
        }
    }

    remove () {
        instance = null;
        let elementForRemoval = document.querySelector('.app-section-join-form');
        elementForRemoval.parentNode.removeChild(elementForRemoval);
    }
}

class SectionCreatorFactory {
    create (type) {
        switch(type) {
            case 'standard': {return new SingleSection('standard');};
            case 'advanced': return new SingleSection('advanced');
        }

    }
}

function createSection (type) {
    if ((type !== 'advanced') && (type !== 'standard')) {
        return null;
    }
    const block = document.createElement('section');
    block.className = 'app-section-join-form';
    block.style.backgroundImage = "linear-gradient(to top, rgba(65, 65, 65, .5), rgba(65, 65, 65, .5)), url(./assets/images/form.png)"
    let blockTitle = document.createElement('h1');
    blockTitle.className = 'app-title';
    blockTitle.textContent = 'Join Our Program';
    blockTitle.style.color = 'white'
    blockTitle.style.margin = '0px'

    let blockText = document.createElement('p');
    blockText.className = 'app-subtitle';
    blockText.innerHTML = 'Sed do eiusmod tempor incididunt<br>ut labore et dolore magna aliqua.';
    blockText.style.color = 'white';
    blockText.style.margin = '40px';

    let blockForm = document.createElement('form');
    blockForm.style.display = 'flex';
    blockForm.style.justifyContent = 'center';
    blockForm.style.marginTop = '10px';
    let blockFormInput = document.createElement('input');
    let blockFormButton = document.createElement('button');
    blockForm.appendChild(blockFormInput);
    blockForm.appendChild(blockFormButton);
    blockFormInput.style.placeContent = 'E-mail';
    blockFormInput.style.backgroundColor = 'rgba(255, 255, 255, 0.4)';
    blockFormInput.style.color = 'white';
    blockFormInput.style.width = '400px';
    blockFormInput.placeholder = 'E-mail';
    blockFormButton.className = 'app-section__button';
    blockFormButton.style.fontSize = '14px';
    blockFormButton.style.height = '36px';
    blockFormButton.style.width = '110px';
    blockFormButton.style.borderRadius = '18px';
    blockFormButton.style.marginLeft = '30px';
    blockFormButton.style.letterSpacing = '1.5px';
    blockFormButton.textContent = 'SUBSCRIBE';

    block.style.height = '436px';
    block.style.paddingTop = '100px';
    block.style.boxSizing = 'border-box';
    block.appendChild(blockTitle);
    block.appendChild(blockText);
    block.appendChild(blockForm);
    blockForm.addEventListener('submit', (e) => {
        e.preventDefault();
        console.log(blockFormInput.value);
        alert(validate(blockFormInput.value));
        removeJoinUsSection();
    });
    if (type === 'advanced') {
        blockFormButton.style.width = '300px';
        blockFormButton.textContent = 'SUBSCTIBE TO OUR ADVANCED PROGRAM';
    }
    return block;
}

function createJoinUsSection () {
    const factory = new SectionCreatorFactory();
    let instance = factory.create('standard').section;
    footer.before(instance);
}

function removeJoinUsSection () {
    instance.remove();
}

export { createJoinUsSection, removeJoinUsSection, instance };
